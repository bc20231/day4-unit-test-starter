package com.afs.unittest;

public class FakeProjectClient implements IProjectClient {
    private boolean returnValue;

    @Override
    public boolean isProjectTypeValid(ProjectType projectType) {
        return false;
    }

    @Override
    public boolean isExpired(Project project) {
        return returnValue;
    }

    public void setReturnValue(boolean returnValue) {
        this.returnValue = returnValue;
    }
}
